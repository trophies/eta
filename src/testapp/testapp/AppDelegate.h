//
//  AppDelegate.h
//  testapp
//
//  Created by Wassim Omais on 1/26/16.
//  Copyright © 2016 Wassim Omais. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

