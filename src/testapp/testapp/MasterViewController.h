//
//  MasterViewController.h
//  testapp
//
//  Created by Wassim Omais on 1/26/16.
//  Copyright © 2016 Wassim Omais. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

