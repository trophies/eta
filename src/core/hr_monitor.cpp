#include "hr_monitor.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

void hr_monitor::insert(int next) {
	_heart_rates.push_back(next);
	if (_heart_rates.size() == _hr_lim + 1)
		_heart_rates.pop_front();
	
	_get_trends();
}

void hr_monitor::_get_trends() {
	_trends.clear();
	_diffs.clear();
	for (int i = 0; i + 1 < _heart_rates.size(); i++) {
		const int diff = _heart_rates[i + 1] - _heart_rates[i];
		_diffs.push_back(diff);
	}

	for (int i = 0; i < _diffs.size(); i++) {
		if (_diffs[i] < 0)
			_trends.push_back(-1);
		else if (_diffs[i] > 0)
			_trends.push_back(1);
		else
			_trends.push_back(0);
	}

	assert(_trends.size() == _diffs.size());
}

int hr_monitor::check_state() {
	const int size = static_cast<int>(_heart_rates.size());
	if (__builtin_expect(size == 0, 0))
		return STABLE;

	// Pass 1: Check for abnormal numbers:
	const int max = 220 - _age;
	const int min = 60;
	for (int i = size - 1; i >= 0; i--) {
		if (_heart_rates[i] >= max)
			return HIGH_RATE;
		if (_heart_rates[i] <= min)
			return LOW_RATE;
	}

	// Pass 2: Analyze diffs:
	const int MAX_DIFF = 10;
	for (int i = _diffs.size() - 1; i >= 0; i--) {
		const int d = std::abs(_diffs[i]);
		if (_diffs[i] < 0 && d > MAX_DIFF)
			return ABNORMAL_DECREASE;
		if (_diffs[i] > 0 && d > MAX_DIFF)
			return ABNORMAL_INCREASE;
	}

	// Pass 3: Get trends:
	int curr_trend = !_trends.empty() ? _trends[0] : -1;
	int trend_diffs = 0;
	for (int i = _trends.size() - 1; i >= 0; i--) {
		if (_trends[i] == 0)
			continue;

		if (_trends[i] != curr_trend) {
			const int d = std::abs(_diffs[i]);
			trend_diffs += (d >= 5);
		}

		curr_trend = _trends[i];
	}

	if (trend_diffs > 3)
		return ABNORMAL_FLUCT;
	
	// Healthy:
	if (__builtin_expect(_heart_rates.size() == 1, 0))
		return STABLE;

	const int last = _heart_rates.back();
	const int nxt_last = _heart_rates[size - 2];
	if (last > nxt_last)
		return INCREASE_OK;
	if (last < nxt_last)
		return DECREASE_OK;

	return STABLE;
}

void hr_monitor::clear() {
	_heart_rates.clear();
	_trends.clear();
	_diffs.clear();
}

inline std::deque<int> hr_monitor::heart_queue() {
	return _heart_rates;
}

int get_heart_rate()
{
	return rand() % 100 + 60;
}

#include <unistd.h>

int main() {
	srand(time(nullptr));
	hr_monitor monitor(20, 50); // A 50 year old:
	while (1) {
		int state = monitor.check_state();
		if (!hr_monitor::is_healthy(state))
			std::cout << hr_monitor::state_string(state) << std::endl;

		int next_rate = get_heart_rate();
		std::cout << "NEXT RATE: " << next_rate << std::endl;
		monitor.insert(next_rate);
		sleep(1);
	}

	return 0;
}
